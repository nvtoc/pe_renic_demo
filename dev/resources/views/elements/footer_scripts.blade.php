<script>            
    var facial_url = '<?= env('APP_ENV') == 'local' ? url('/pages/facial_data') : secure_url('/pages/facial_data') ?>';
    var signer_url = '<?= env('APP_ENV') == 'local' ? url('/pages/sign_document') : secure_url('/pages/sign_document') ?>';        
    var register_fail_url = '<?= env('APP_ENV') == 'local' ? url('/pages/register_fail') : secure_url('/pages/register_fail') ?>';
</script>

<script src="https://cdn.webrtc-experiment.com/DetectRTC.js"></script>
<script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>

<script src="assets/jquery/jquery-3.2.1.min.js"></script>
<script src="assets/jquery-easing/jquery.easing.min.js"></script>
<script src="assets/bootstrap-4.3.1/js/bootstrap.min.js"></script>
<script src="assets/jquery-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script src="assets/bootbox5/bootbox.min.js"></script>
<script src="assets/localize/jquery.localize.min.js"></script>
<script src="assets/image-picker/js/image-picker.min.js"></script>
<script src="assets/sweetalert/sweetalert.min.js"></script>
<script src="js/sign_document.js"></script>
<script src="js/ci_codes.js"></script>
<script src="locale/lang.js"></script>
<script src="js/status.js"></script>
<script src="js/toc_utils.js"></script>
<script src="js/ui_interactions.js"></script>    
<script src="js/app.js"></script>