@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <div class="loading">Loading&#8230;</div>
        <!-- Inicio compatibilidad navegadores -->
        <div class="row supported" style="display:none;">
            <div class="col-lg">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center" style="margin-bottom:0px;" data-localize="supported-browsers">
                            TOC API Facial - Navegadores compatibles: Chrome, Mozilla y Opera (actualizados últimas versiones)</h4>
                        <h5 class="text-center" data-localize="supported-browsers-detail">Para mayor información sobre soporte, revisar <a href="https://webrtc.org/">https://webrtc.org/</a></h5>
                        <hr>
                        <div class="row justify-content-center browsers">
                            <div class="col-sm-3">
                                <img src="img/chrome.png" alt="">
                            </div>
                            <div class="col-sm-3">
                                <img src="img/firefox.png" alt="">
                            </div>
                            <div class="col-sm-3">
                                <img src="img/opera.svg" class="opera" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin compatibilidad navegadores -->
        <!-- Inicio API -->
        <div class="api">
            <canvas id="photo" class="hide" style="display:none;" height="720" width="1280"></canvas>
            <canvas id="rotated" class="hide" style="display:none;" height="720" width="1280"></canvas>
            <img id="camera" src="" alt="" style="display:none">
            <div class="row">
                <div class="col-md-9 mx-auto">
                    <!-- Inicio Paso a Paso -->
                    <div id="rootwizard">
                        <!-- Inicio Navegación -->
                        <div class="row">
                            <div class="col">
                                <ul class="" id="apiTab" role="tablist">
                            	  	<li class="nav-item">
                                        <a class="" id="intro-tab" href="#intro" data-toggle="tab">
                                            <span class="badge badge-pill badge-dark active">1</span></a>
                                    </li>
                            		<!-- <li class="nav-item">
                                        <a class="" id="document-tab" href="#document" data-toggle="tab">
                                            <span class="badge badge-pill badge-outline-dark">2</span></a>
                                    </li> -->
                            		<li class="nav-item">
                                        <a class="" id="front-tab" href="#front" data-toggle="tab">
                                            <span class="badge badge-pill badge-outline-dark">3</span></a>
                                    </li>
                            		<!-- <li class="nav-item">
                                        <a class="" id="back-tab" href="#back" data-toggle="tab">
                                            <span class="badge badge-pill badge-outline-dark">4</span></a>
                                    </li> -->
                            		<li class="nav-item">
                                        <a class="" id="selfie-tab" href="#selfie" data-toggle="tab">
                                            <span class="badge badge-pill badge-outline-dark">5</span></a>
                                    </li>
                            		<li class="nav-item">
                                        <a class="" id="result-tab" href="#result" data-toggle="tab">
                                            <span class="badge badge-pill badge-outline-dark">6</span></a>
                                    </li>
                            	</ul>
                            </div>
                        </div>
                        <!-- Fin Navegación -->
                        <!-- Inicio Selección de cámaras -->
                        <div class="row">
                            <div class="col">
                                <div class="form-inline cameras" style="margin-bottom: 1%;">
                                    <label for="videoSource" style="margin-right: 10px; " data-localize="select-camera">Seleccionar Cámara:</label>
                                    <select id="videoSource" class="custom-select col-6"></select>
                                </div>
                            </div>
                        </div>
                        <!-- Fin Selección de cámaras -->
                        <div class="row">
                            <div class="col">
                                <!-- Inicio Tabs -->
                                <div class="tab-content">
                                    <!-- Inicio paso introducción -->
                            	    <div class="tab-pane fade" role="tabpanel" id="intro">
                                        <fieldset>
                                            <h2 class="fs-title" data-localize="intro-title">&nbsp;</h2>
                                            <h3 class="fs-subtitle"><i data-localize="intro-subtitle">&nbsp;</i></h3>
                                            <hr>
                                            <p data-localize="select-verification">Seleccione tipo de verificación de identidad</p>
                                            <div class="picker verficationType" style="margin: 0 auto;text-align: center;display: table;">
                                                <select id="verificationType" class="image-picker show-html">
                                                    <option value=''></option>
                                                    <option data-img-src="img/peru/blue.png" value="PER1"></option>
                                                    <option data-img-src="img/peru/white.png" value="PER2"></option>
                                                </select>
                                            </div>
                                        </fieldset>
                            	    </div>
                                    <!-- Fin paso introducción -->
                                    <!-- Inicio Selección documento -->
                            	    <!-- <div class="tab-pane fade" role="tabpanel" id="document">
                                        <fieldset id="document">
                                            <h2 class="fs-title" data-localize="document">Tipo de Documento de Identidad</h2>
                                            <h3 class="fs-subtitle" data-localize="select-document-type">Seleccione el tipo de documento para verificación.</h3>
                                            <br>
                                            <hr>
                                            <div class="row justify-center">
                                                <div class="col"></div>
                                                <div class="col-4">
                                                    <label class="custom-control custom-toggle" style="text-align: left;">
                                                        <input name="allCI" type="checkbox" value="" class="custom-control-input" id="checkbox">
                                                        <span class="custom-control-indicator"></span>
                                                        <span class="custom-control-description" data-localize="all-documents"> Todos los documentos</span>
                                                    </label>
                                                </div>
                                                <div class="col-7">
                                                    <label for="documentType" class="col-form-label" data-localize="document-type" style="margin-right:8px;">Tipo Documento</label>
                                                    <select class="custom-select col-6" id="documentType" style="margin-right:8px;">
                                                        <option value="0">Seleccione Tipo CI</option>
                                                        <option value="CHL1">CI Antiguo</option>
                                                        <option value="CHL2">CI Nuevo</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                        </fieldset>
                            	    </div> -->
                                    <!-- Fin Selección documento  -->
                                    <!-- Inicio Foto frontal -->
                            		<div class="tab-pane fade" role="tabpanel" id="front">
                                        <fieldset id="front">
                                            <h2 class="fs-title" data-localize="front-id">Foto Documento Frontal</h2>
                                            <h3 class="fs-subtitle" style="display:inline;" data-localize="front-id-subtitle">Toma una foto de su cédula por el frente.</h3>
                                            <img class="check-orientation" style="width:15%; display:none;" src="img/doc-ok.png" alt="">
                                            <div class="image">
                                                <div class="row justify-content-center">
                                                    <div class="col-1 z-top">
                                                        <a href="javascript:void(0);" data-dir="left" class="btn btn-sm btn-dark rotate float-left">
                                                            <i class="fa fa-undo" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div class="col-9 text-center">
                                                        <input type="text" name="id_front" value="" hidden>
                                                        <img id="id_front" class="img-fluid" src="">
                                                        <div id="front-video">
                                                            <div id="video-stream">
                                                                <video id="video" style="max-width:100%;"></video>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1 z-top">
                                                        <a href="javascript:void(0);" data-dir="right" class="btn btn-sm btn-dark rotate float-right">
                                                            <i class="fa fa-repeat" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="file" id="doc-front" name="doc-front" class="inputfile" accept="image/*" capture="camera">
                                            <label for="doc-front" class="images captura-btn"><i class="fa fa-camera" aria-hidden="true"></i> <span data-localize="capture">FOTO</span></label>
                                            <a href="#" class="btn btn-secondary take-photo"><i class="fa fa-camera" aria-hidden="true"></i> <span data-localize="capture">FOTO</span></a>
                                        </fieldset>
                            	    </div>
                                    <!-- Fin foto frontal -->
                                    <!-- Inicio foto reverso -->
                            		<!-- <div class="tab-pane fade" role="tabpanel" id="back">
                                        <fieldset id="back">
                                            <h2 class="fs-title" data-localize="back-id">Foto Documento Reverso</h2>
                                            <h3 class="fs-subtitle" style="display:inline;" data-localize="back-id-subtitle">Toma una foto de su cédula por el reverso.</h3>
                                            <img class="check-orientation" style="width:15%; display:none;" src="img/doc-back-ok.png" alt="">
                                            <div class="image">
                                                <div class="row">
                                                    <div class="col-1">
                                                        <a href="javascript:void(0);" data-dir="left" class="btn btn-sm btn-dark rotate float-left">
                                                            <i class="fa fa-undo" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div class="col-10">
                                                        <input type="text" name="id_back" value="" hidden>
                                                        <img id="id_back" class="img-fluid" style="margin: 1%;" src="" alt="Back">
                                                        <div id="back-video"></div>
                                                    </div>
                                                    <div class="col-1">
                                                        <a href="javascript:void(0);" data-dir="right" class="btn btn-sm btn-dark rotate float-right">
                                                            <i class="fa fa-repeat" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="file" id="doc-back" name="doc-back" class="inputfile" accept="image/*" capture="camera">
                                            <label for="doc-back" class="images captura-btn"><i class="fa fa-camera" aria-hidden="true"></i> <span data-localize="capture">FOTO</span></label>
                                            <a href="#" class="btn btn-secondary take-photo"><i class="fa fa-camera" aria-hidden="true"></i> <span data-localize="capture">FOTO</span></a>
                                        </fieldset>
                            	    </div> -->
                                    <!-- Fin Foto reverso -->
                                    <!-- Inicio Selfie -->
                            		<div class="tab-pane fade" role="tabpanel" id="selfie">
                                        <fieldset id="selfie" class="fieldset-rostro">
                                            <h2 class="fs-title" data-localize="selfie-title">Foto Rostro</h2>
                                            <h3 class="fs-subtitle" style="display:inline;" data-localize="selfie-subtitle">Toma una foto de su rostro.</h3>
                                            <img class="check-orientation" style="width:8%; margin-left:5px; display:none;" src="img/selfie-ok.png" alt="">
                                            <div class="image">
                                                <div class="row justify-content-center">
                                                    <div class="col-1 z-top">
                                                        <a href="javascript:void(0);" data-dir="left" class="btn btn-sm btn-dark rotate float-left">
                                                            <i class="fa fa-undo" aria-hidden="true"></i></a>
                                                    </div>
                                                    <div class="col-9 text-center">
                                                        <input type="text" name="selfie" value="" hidden>
                                                        <img id="id_selfie" class="img-fluid" style="margin: 1%;" src="" alt="Selfie">
                                                        <div id="selfie-video"></div>
                                                    </div>
                                                    <div class="col-1 z-top">
                                                        <a href="javascript:void(0);" data-dir="right" class="btn btn-sm btn-dark rotate float-right">
                                                            <i class="fa fa-repeat" aria-hidden="true"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="file" name="selfie-photo" id="selfie-photo" class="inputfile" accept="image/*" capture="camera">
                                            <label for="selfie-photo" class="images captura-btn"><i class="fa fa-camera" aria-hidden="true"></i> <span data-localize="capture">FOTO</span></label>
                                            <a href="#" class="btn btn-secondary take-photo"><i class="fa fa-camera" aria-hidden="true"></i> <span data-localize="capture">FOTO</span></a>
                                        </fieldset>
                            	    </div>
                                    <!-- Fin Selfie -->
                                    <div class="tab-pane fade" role="tabpanel" id="result">
                                        <fieldset>
                                            <h2 class="fs-title"><span data-localize="result-title">Resultado</span></h2>
                                            <h3 class="fs-subtitle" data-localize="result-subtitle">Resultado verificación facial</h3>
                                            <div class="result">
                                                <div class="row">
                                                    <div class="col-8 mx-auto">
                                                        <div class="card match">
                                                            <div class="card-body">
                                                                <!-- Token transacción -->
                                                                <h5 class="modal-title token">Token: <span class="badge badge-light" id="token-value"></span></h5>
                                                                <hr>
                                                                <!-- ícono resultado positivo biometría 2 -->
                                                                <div class="positive" style="display:none;">
                                                                    <div id="success">
                                                                        <svg class="tickmark" viewBox="0 0 52 52">
                                                                            <circle class="circle" cx="26" cy="26" r="25" fill="none" />
                                                                            <path class="tick" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                                <!-- ícono resultado positivo biometría 1 -->
                                                                <div class="warning" style="display:none;">
                                                                    <div id="info">
                                                                        <svg class="tickmark" viewBox="0 0 52 52">
                                                                            <circle class="circle" cx="26" cy="26" r="25" fill="none" />
                                                                            <path class="tick" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" />
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                                <!-- ícono resultado negativo biometría -->
                                                                <div class="negative" style="display:none;">
                                                                    <div id="error">
                                                                        <svg class="crossmark" viewBox="0 0 52 52">
                                                                            <circle class="circle" cx="26" cy="26" r="25" fill="none" />
                                                                            <line class="cross" x1="14.1" y1="15.1" x2="37.5" y2="38.6" />
                                                                            <line class="cross late" x1="37.6" y1="15.2" x2="14.1" y2="38.5" />
                                                                        </svg>
                                                                    </div>
                                                                </div>
                                                                <br>
                                                                <!-- Resultado biométrico -->
                                                                <h5 class="fs-title text-center">
                                                                    <span data-localize="match-facial">Verificación:</span> <span class="confidence"></span></h5>
                                                                <!-- datos del documento -->
                                                                <div class="info-doc">
                                                                    <br>
                                                                    <h5 class="card-title text-center" data-localize="document-info">Información Documento</h5>                                                                    
                                                                    <ul class="list-group">                                                                                                                                                
                                                                        <li class="list-group-item names"><span data-localize="names">Nombres: </span><strong id="firstname"></strong></li>
                                                                        <li class="list-group-item lastnames"><span data-localize="lastnames">Apellidos: </span><strong id="lastname"></strong></li>                                                                        
                                                                    </ul>
                                                                    <!-- <ul class="list-group data" style="display:none;"></ul> -->
                                                                </div>
                                                                <div class="verify">
                                                                    <ul class="list-group">                                                                                                                                                
                                                                        <li class="list-group-item"><span>Selfie vs Reniec: </span><strong id="biometric_result"></strong></li>
                                                                        <li class="list-group-item"><span>Front vs Selfie: </span><strong id="front_selfie_result"></strong></li>                                                                        
                                                                        <li class="list-group-item"><span>Front vs Reniec: </span><strong id="front_reniec_result"></strong></li>                                                                        
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                            	    </div>
                                    <!-- Fin Resultado -->
                            	</div>
                                <!-- Fin Tabs -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <!-- Inicio botones Navegación -->
                                <ul class="pager wizard">
                                    <li class="previous"><a class="btn btn-outline-secondary btn-pill prev" href="#" data-localize="previous">Anterior</a></li>
                                    <li class="next"><a id="next-btn" class="btn btn-secondary btn-pill next" href="#" data-localize="next">Siguiente</a></li>
                                    <li><a class="btn btn-secondary btn-pill api-call" href="#" data-localize="verification">VERIFICAR</a></li>
                                    <li><a class="btn btn-secondary btn-pill again" href="#" data-localize="new-verification">Realizar Nueva Verificación</a></li>
                                </ul>
                                <!-- Fin botones Navegación -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>

    
    <!-- MODAL Estado CI SRCEL -->
    <div class="modal fade in" aria-hidden="false" style="display: none; padding-left: 17px;" id="modal-estado">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Estado de la Cédula</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Cerrar</span>
                    </button>
                </div>
                <div class="modal-body"><span id="estadoCIMensaje"></span></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary bfd-ok" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>

        <!-- MODAL Estado PC Info -->
        <div class="modal fade in" aria-hidden="false" style="display: none; padding-left: 17px;" id="modal-equipo">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Información del PC</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Cerrar</span>
                        </button>
                    </div>
                    <div class="modal-body"><span id="pcInfoMessage"></span></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary bfd-ok" data-dismiss="modal">Aceptar</button>
                    </div>
                </div>
            </div>
        </div>

    <!-- MODAL NIST -->
    <div class="modal fade in" aria-hidden="false" style="display: none; padding-left: 17px;" id="modal-nist">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Índice NIST</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Cerrar</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>El índice NIST indica la calidad de la imagen capturada de la huella. <br>Este índice se mide de la siguiente forma: </span>
                    <ol>
                        <li>Muy bueno</li>
                        <li>Bueno</li>
                        <li>Regular</li>
                        <li>Malo</li>
                        <li>Muy malo</li>
                    </ol>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary bfd-ok" data-dismiss="modal">Aceptar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Inicio Facial -->
    <div class="modal" tabindex="-1" role="dialog" id="modal-confirm-facial">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Error en proceso de verificación</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Cerrar</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="msj-error-verificacion"></p>
                    <p>¿Desea realizar una verificación de identidad facial?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="confirm-facial-goes">Sí</button>
                    <button type="button" class="btn btn-secondary" id="discard-facial" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL INFO Facial -->
    <div class="modal fade in" aria-hidden="false" style="display: none; padding-left: 17px;" id="modal-facial">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Información de la verificación</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true">×</span><span class="sr-only">Cerrar</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <div class="card match">
                                <div class="card-body">
                                    <h5 class="text-center token">Token: <span class="badge badge-light"></span></h5>
                                    <hr>
                                    <div class="text-center">
                                        <span id="feedbackFacial"></span>
                                    </div>
                                    <div class="info-doc">
                                        <h4 class="text-center">Información Documento</h4>
                                        <ul class="list-group mrz-data">
                                            <li class="list-group-item rut"><span>Rut:</span>
                                                <strong></strong>
                                            </li>
                                            <li class="list-group-item n-serie"><span>N° Serie:</span>
                                                <strong></strong>
                                            </li>
                                            <li class="list-group-item names"><span>Nombres:</span>
                                                <strong></strong>
                                            </li>
                                            <li class="list-group-item lastnames"><span>Apellidos:</span>
                                                <strong></strong>
                                            </li>
                                            <li class="list-group-item gender"><span>Genero:</span>
                                                <strong></strong>
                                            </li>
                                            <li class="list-group-item country"><span>Nacionalidad:</span>
                                                <strong></strong>
                                            </li>
                                            <li class="list-group-item date-birth"><span>Fecha Nacimiento:</span>
                                                <strong></strong>
                                            </li>
                                            <li class="list-group-item date-expire"><span>Fecha Vencimiento:</span>
                                                <strong></strong>
                                            </li>
                                        </ul>
                                        <ul class="list-group data" style="display:none;"></ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- MODAL Estado dispositivos -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Estado de los dispositivos</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-striped" style="font-size: 15px !important">
                                <thead>
                                    <tr>
                                        <th>Dispositivo</th>
                                        <th style="text-align: center">Estado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Lector de huella</td>
                                        <td style="text-align: center">
                                            <img class="loader icon-toc" height="50" src="img/spinner.gif" id="fingerPrintStatus">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lector de código</td>
                                        <td style="text-align: center">
                                            <img class="loader icon-toc" height="50" src="img/spinner.gif" id="barCodeStatus">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Lector de tarjeta</td>
                                        <td style="text-align: center">
                                            <img class="loader icon-toc" height="50" src="img/spinner.gif" id="nfcCardStatus" >
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Inicio Modal error resultado facial -->
    <div class="modal fade" id="error_facial" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mx-auto"><span>Verificación</span>: <span class="text-danger">Negativa</span></h5>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <!--<div class="container mb-2">
                            <span>Ocurrió un error en el procesamiento de las siguitenes imágenes: </span>
                            <strong><span id="errorPhotoCount"></span></strong>
                            <span>Verifica cuál tuvo el error y vuelve a tomar la foto.</span>
                        </div>-->
						<div class="container mb-2">
                            <span>Ocurrió un error en el procesamiento de una o las imágenes</span>
                            <!--<strong><span id="errorPhotoCount"></span></strong>-->
                            <span>vuelve a tomar la foto.</span>
                        </div>
                        <h4 class="card-title text-center">Resultado Verificación</h4>
                        <ul class="list-group errorPhoto">
                            <li class="list-group-item front" id="listFrontRepit">
                                <span>FOTO FRONTAL</span>:<i class="status"></i>
                                <a class="btn btn-sm btn-outline-secondary photo-error" href="#" style="display:none">
                                    <i class="fa fa-camera"></i> <span>Capturar</span></a>
                            </li>
                            <li class="list-group-item back" id="listBackRepit">
                                <span>FOTO REVERSO</span>:<i class="status"></i>
                                <a class="btn btn-sm btn-outline-secondary photo-error" href="#" style="display:none">
                                    <i class="fa fa-camera"></i> <span>Capturar</span></a>
                            </li>
                            <li class="list-group-item selfie" id="listSelfieRepit">
                                <span>FOTO SELFIE</span>:<i class="status"></i>
                                <a class="btn btn-sm btn-outline-secondary photo-error" href="#" style="display:none">
                                    <i class="fa fa-camera"></i> <span>Capturar</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal errores resultado facial -->


    <div id="pdf-content" class="text-center mt-4 hide">
        <div class="text-center"><button class="btn btn-primary mb-4" onClick="window.location.reload();" id="btn-reset-page">Volver a verficar</button></div>        
        <object id="pdf-signed" data="" type="application/pdf" width="650px" height="800px"></object>                        
    </div>


@endsection