<?php

namespace App\Tools;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

class ReniecTools 
{

   private $url;
   private $api_key;
   private $url_face;
   private $token_liveness;
   private $path_document = 'doc/demo_reniec.pdf'; 
   protected $connection = null;

   public function __construct()
   {                  
      Log::info('ReniecTools __construct');      
      $this->api_key = \Config::get('facial.FACIAL_API_KEY');
      $this->url = \Config::get('facial.URL_SIGNER');      
      $this->url_face = \Config::get('facial.URL_FACE');
      $this->tokenLiveness = \Config::get('facial.TOKEN_LIVENESS');            
   }



   public function callReniec($front, $selfie, $dni, $document_type, $sign_document, $liveness = false){

      Log::info('ReniecTools callReniec');            
      
      $curl = curl_init();
      $front = file_get_contents($front);
      $selfie = $liveness ? $selfie : file_get_contents($selfie);
      
      $array = [
        'apiKey' => $this->api_key,        
        'dni'    => $dni,
        'selfie' => $selfie,
        'document_type' => $document_type,
        'id_front' => $front
      ];
  
      curl_setopt_array($curl, array(
        CURLOPT_URL => $this->url_face,
        CURLOPT_RETURNTRANSFER => true,
         //   CURLOPT_ENCODING => "",
         //   CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 60,
        // CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_SSL_VERIFYPEER => false,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $array,
        CURLOPT_HTTPHEADER => array(
          "content-type: multipart/form-data"
        ),
      ));
      
      $response = curl_exec($curl);
      $err = curl_error($curl);    
      curl_close($curl);            
      
      Log::info(print_r($response, true));
      Log::info(print_r($err, true));                  

      if ($err) {
        return ['status' => false, 'msg' => 'err'];
      } else {      
        try {
            $json_data = json_decode($response, true);
            Log::info("Enviar datos de vuelta");      
            return ['status' => true, 'data' => $json_data];
        } catch (\Throwable $th) {
            Log::info("Error parser");      
            return ['status' => false, 'msg' => 'catch'];
        }  
      }
  }
  

   // Firma Dactilar o Facial
   public function signDocument($type = null, $toc_token, $rut = null){      

      if($type == 1){
         Log::info('Tipo 1: dactilar');
         $data = $this->_signCommon($type, $toc_token, $rut);
      }
      else{
         Log::info('Tipo 2: facial');
         $data = $this->_signCommon($type, $toc_token);
      }
      return $data;
   }


   
   private function _signCommon($type, $toc_token, $rut = null){
                
      $data = ['status' => false, 'data' => null, 'msg' => 'Error de firma','result'=> 0, 'type' => 1, 'register' => null];
      $new_register = ['token_verification'=> $toc_token, 'rut' => $rut, 'verification' => 1];    
   
      try {         
         $campos_firma = [
            'page' => 1,
            'pos_x' => '150',
            'pos_y' => '200'
         ];
   
         $array = [
            'apiKey' => $this->api_key,
            'pdf' => file_get_contents($this->path_document),
            'toc_token' => $toc_token,
            'page' => $campos_firma['page'],
            'pos_x' => $campos_firma['pos_x'],
            'pos_y' => $campos_firma['pos_y'],			            
         ];

         if($type == 1){
            $array['rut'] = $rut;
         }
      
         Log::info('Antes de Firmar');
         
         $curl = curl_init();
         curl_setopt_array($curl, array(
            CURLOPT_URL => $this->url,
            CURLOPT_RETURNTRANSFER => true,
            //CURLOPT_ENCODING => "",
            //CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_POSTFIELDS => $array,
            CURLOPT_HTTPHEADER => array(
               "content-type: multipart/form-data"
            ),
         ));
               

         $response = curl_exec($curl);
         $err = curl_error($curl);             
         curl_close($curl);
         
         Log::info('Después de Firmar');
         Log::info(print_r($response, true));         
         Log::info(print_r($err, true));                  

         if(!$err){          
            Log::info("Curl sin error");    
            
            if($response){
               $filename = 'doc/signed/'.$toc_token.'.pdf';   
               $data_json = json_decode($response, true);    
               $new_register['status_signed'] = $data_json['status'];            
                           
               Log::info(print_r($data_json, true));     
               if($data_json && isset($data_json['status']) && $data_json['status'] == '200'){   
                  
                  Log::info("Estado 200");        
                  $new_register['token_signed'] = $data_json['toc_token'];
                  $new_register['doc_url'] = $filename;
   
                  if(isset($data_json['signed pdf'])){
   
                     // escribir archivo
                     $content = base64_decode($data_json['signed pdf']);
                     $file = fopen($filename, "wb");
                     fwrite($file, $content);
                     fclose($file);
   
                     if(file_exists($filename)){
                        $data['status'] = true;
                        $data['data'] = $data_json;
                        $data['msg'] = 'Firma OK';
                        $data['url'] = $filename; 
                        $new_register['result'] = 1;                                             
   
                     }    
                     else{
                        Log::info('No se encontró archivo pdf');   
                     }           
      
                  }
                  else{
                     Log::info('No se encontró signed pdf');
                  }
               }    
               else{
                  Log::info("Estado !=200");
               }     
            }
            else{
               Log::info("No viene Response");
            }
         }   
         else{
            Log::info('Error curl ...');
            Log::info($curl_response);
            // Log::info(print_r($err, true));   
         }                           
      } catch (\Throwable $th) {
         Log::info('Error catch en firma...');
         Log::info($th);
      }
      
      $data['register'] = $new_register;
      return $data;
   }
   

   // Generar Token para liveness
   public function getTokenLiveness(){
      $apiKey = $this->api_key;
      $url = $this->token_liveness;
      $post = array('apiKey' => $apiKey);
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_POST, 1);
      curl_setopt($curl, CURLOPT_TIMEOUT, 30);
      curl_setopt($curl, CURLOPT_POST, count($post));
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
      curl_setopt($curl, CURLOPT_VERBOSE,true);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);        
      $result = curl_exec($curl);
      if(!$result){
         Log::info('Error: "' . curl_error($curl) . '" - Code: ' . curl_errno($curl));                  
      }      
      curl_close($curl);      
      return $result;
   }


}

