<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Tools\SimpleSign;

class SimpleSignServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind(SimpleSign::class, function ($app) {
        //     return new SimpleSign();
        // });
        $this->app->singleton('App\Tools\SimpleSign', function ($app) {
            return new SimpleSign();
        });

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
