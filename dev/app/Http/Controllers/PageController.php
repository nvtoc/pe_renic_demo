<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tools\ReniecTools;
use Illuminate\Support\Facades\Log;
use App\Register;

class PageController extends Controller
{
    
    public function index(){
        return view('pages.index');
    }

    public function facial(){
        return view('pages.facial');
    }

    public function _facial_data(Request $request){        

        try {            
            if($request->files){                           
                $reniec = new ReniecTools();
                $withSign = isset($request['signDocument']) && $request['signDocument'] == "true" ? true : false;                     

                if($_POST['liveness'] == "true"){
                    
                    Log::info('Liveness vs Reniec');      
                    
                    // Liveness reniec
                    $photo1 = $_FILES["photo1"]['tmp_name'];
                    $photo2 = $request["photo2"];
                    $response = $reniec->callReniec($photo1, $photo2, $request['dni'], $request['documentType'], $withSign, false);

                    Log::info("Respuesta callReniec con Livenes");
                    Log::info(print_r($response));

                    if($response && isset($response['status'])){                                                                        
                        echo json_encode($response);                        
                    }
                    else{
                        Log::info("APi no contesto correctamente");                        
                        echo json_encode(['status' => false, 'msg' => 'Error: faltan parámetros']);
                    }
                }
                else{
                    // Foto vs Foto renic		
                    Log::info('Foto vs Reniec');                          
                    $photo1 = $_FILES["photo1"]['tmp_name'];
                    $photo2 = $_FILES["photo2"]['tmp_name'];                
                    $response = $reniec->callReniec($photo1, $photo2, $request['dni'], $request['documentType'], $request['signDocument']);  
                    
                    Log::info("Respuesta callReniec sin Livenes");                                        
                    Log::info(print_r($response, true));      
                    if($response && isset($response['status'])){                                                
                        echo json_encode($response, true);                        
                    }
                    else{
                        Log::info("API no contesto correctamente");                        
                        echo json_encode(['status' => false, 'msg' => 'Error consumo API-RENIEC']);
                    }
                }            
            } else {                
                echo json_encode(['status' => false, 'msg' => 'Error: faltan parámetros']);
            }                    
        } catch (\Throwable $th) {
            //throw $th;            
            echo json_encode(['status' => false, 'msg' => 'Error exception']);
        }
                
    }

    public function _sign_document(Request $request){

        Log::info('sign_document pageController');
        $type = $request->input('type');
        $token = $request->input('token');
        $rut = $request->input('rut');    

        // sign and return 
        $simpleSign = new ReniecTools();
        $doc = $simpleSign->signDocument($type, $token, $rut);
        echo json_encode($doc);   

    }


    public function _register_fail(Request $request){
        $type = $request->input('type');
        $token = $request->input('token');
        $rut = $request->input('rut');    
        $verification = $request->input('verification');    
        
        $register = new Register();        
        $register->rut = $rut;     
        $register->type = $type;                                
        $register->token_verification = $token;
        $register->verification = $verification;
        $register->result = 0;
        $register->save();
    }

    public function _get_token(Request $request){
        $reniec = new ReniecTools();
        $token = $reniec->getTokenLiveness();
        return $token;
    }


}
