
function signDocumentFN(type, token, rut){   
    swal({
        title: "Firmando de documento",
        text: '<div class="lds-ellipsis"><div></div><div></div><div></div><div></div></div>',
        type: "info",
        showCancelButton: false,                
        showConfirmButton: false,
        showLoaderOnConfirm: false,
        html: true,
        timer: 0,        
    }, function () {                    
        goSign(type, token, rut);       
    });
}



function registerFail(type, token, rut, verification){                       
    $.ajax({
        url: register_fail_url,
        type: 'post',
        data: { type: type, token: token, rut: rut, verification: verification },
        dataType: 'json'
    })
    .done(function(response){        
        console.log(response);
    })
    .fail(function(error){
        console.log(error);        
    });
}


function goSign(type, token, rut){    
    $.ajax({
        url: signer_url,
        type: 'post',
        data: { type: type, token: token, rut: rut },
        dataType: 'json'
    })
    .done(function(response){        
        if(typeof response =='object' && response['status']){
            swal({
                title: "Documento firmado!", 
                text: false,
                type: "success",
                buttons: {
                        confirm: {
                        text: "Ver documento",
                        value: true,
                        visible: true,
                        className: "",
                        closeModal: true
                    }
                }   

            },function(){                                
                $('#pdf-signed').attr('data', response['url']).removeClass('d-none');                
            });
        }
        else{
            swal("Documento no pude ser firmado!", "", "warning");
        }
    })
    .fail(function(error){
        console.log(error);
        swal("Documento no pude ser firmado!", "", "warning");
    });
}

