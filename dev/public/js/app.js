$('.loading').hide();
$('.again').hide();
$('.result').hide();
$('.info-doc').hide();
$('.cameras').hide();
$('.api-call').hide();
$('#next-btn').hide();

var documentData = {
    'national identification number': '',
    'document number': '',
    'name': '',
    'family name': '',
    'gender': '',
    'nationality': '',
    'date of birth': '',
    'expiration date': ''
};

var cedula_mrz = {
    'national identification number':'',
    'document number':'',
    'name':'',
    'family name':'',
    'gender':'',
    'nationality': '',
    'date of birth':'',
    'expiration date':''
};

var direction = '';
var app_locale = '';
var locale = '';


var documentTypePeru = null;
var documentKind = null;
var cui = null;
function setPeru(){
    documentKind = 'RENIEC';
    $('#documentType').val('PASS');  
    $('#document-tab').parent().hide();
    $('#back-tab').parent().hide();      
}

$(document).ready(function() {
    app_locale = setLanguage();    
    setPeru();
    setNavigationNumbers();    
    
    $('#verificationType').imagepicker({hide_select: true, show_label: false});    
    // setImageSizesVerification();
    $('#rootwizard').bootstrapWizard({
        withVisible: false,
        'tabClass': 'nav nav-pills',
        onTabShow: function(tab, navigation, index) {
            current_tabpane = $('.api').find('.tab-pane.active.show');
            current_tabpane_id = current_tabpane.attr('id');
            $('.pager li').show();
            var stepNumber = index + 1;
            $('ul.nav-pills li a span').removeClass('badge-dark').addClass('badge-outline-dark');
            $('ul.nav-pills li a span').removeClass('active');
            tab.children().children().removeClass('badge-outline-dark').addClass('badge-dark active');
            if (index == 0) {
                $('.pager li.previous').hide();
            } else if (stepNumber == navigation.children().length) {
                $('.pager li.next').hide();
            } else {
                $('.pager li').show();
            }
        },
        onNext: function(tab, navigation, index) { 
            console.log('nex tab index al que voy');
            console.log(index);
        },
        onPrevious: function(tab, navigation, index) { 
            console.log('prev tab index al que voy');
            console.log(index);
        }
    });

    if (!DetectRTC.isMobileDevice) {
        if (DetectRTC.isWebRTCSupported) {
            console.log('Browser Support WebRTC')
            navigator.mediaDevices.enumerateDevices().then(function (devices) {
                for(var i = 0; i < devices.length; i ++){
                    var device = devices[i];
                    if (device.kind === 'videoinput') {
                        var option = document.createElement('option');
                        option.value = device.deviceId;
                        option.text = device.label || 'camera ' + (i + 1);
                        document.querySelector('select#videoSource').appendChild(option);
                    }
                };
                setVideoStream($('#videoSource').val());
            });
            $('input[type="file"]').detach();
            $('label.images').detach();
        } else {
            console.log('This Browser Not Support WebRTC');
            bootbox.alert(lang[app_locale]['webrtc_fail']);
            $('.api').detach();
            $('.supported').show();
        }
    } else {
        $('#video-stream').detach();
        $('.cameras').detach();
        $('a.take-photo.btn.btn-secondary').detach();
    }
});


// -1 No se ha podido encontrar una cara en la imagen enviada.
//  0 Resultado negativo de la verificación.
//  1 Resultado positivo con un 96.5% de confianza.
//  2 Resultado positivo con un 99.43% de confianza.
function score(score){                                   
    var error = '<svg class="crossmark" viewBox="0 0 52 52"><circle class="circle" cx="26" cy="26" r="25" fill="none" /><line class="cross" x1="14.1" y1="15.1" x2="37.5" y2="38.6" /><line class="cross late" x1="37.6" y1="15.2" x2="14.1" y2="38.5" /></svg>';    
    var success1 = '<svg class="tickmark warning" viewBox="0 0 52 52"><circle class="circle" cx="26" cy="26" r="25" fill="none" /><path class="tick" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" /></svg>';
    var success2 = '<svg class="tickmark" viewBox="0 0 52 52"><circle class="circle" cx="26" cy="26" r="25" fill="none" /><path class="tick" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8" /></svg>';
    var value = null;
    
    switch (parseInt(score)) {
        case -1:                                 
            value = error;
            break;            
        case 0:         
            value = error;
            break;
        case 1:               
            value = success1;
            break;
        case 2:              
            value = success2;
            break;    
        default:
            value = error;
            break;
    }

    return value;
}


function mathFacial(selfie_reniec, reniec_front){
    console.log('mathFacial FN');
    console.log(selfie_reniec);
    console.log(reniec_front);
    
    if(parseInt(selfie_reniec) + parseInt(reniec_front) >= 3){
        $('#biometric_result').html(score(2));                                        
    }
    else{
        $('#biometric_result').html(score(0));              
    }    
}



function callAPI(liveness){

    console.log('callAPI FN');
    $('.loading').show();
    var data_valid = false;        
    var id_front = $('#id_front').attr('src');
    var _liveness = typeof liveness !== 'undefined' && liveness ? true : false;    
    var selfie = _liveness ?  $('input[name="selfie"]').val() : $('#id_selfie').attr('src');
    var signDocument = $('#documentSigner').is(':checked');    

    if (id_front.length > 0 && selfie.length > 0) {
        data_valid = true;
    }
    if (data_valid) {
        var form_data = new FormData();
        form_data.append('photo1', dataURItoBlob(id_front));        
        form_data.append('photo2', _liveness ? selfie : dataURItoBlob(selfie));   
        form_data.append('dni', cui);  
        form_data.append('documentType', documentTypePeru);
        form_data.append('liveness', _liveness);        
        form_data.append('signDocument', signDocument);        

        $.ajax({
            url: facial_url,
            async: true,
            crossDomain: true,
            processData: false,
            contentType: false,
            type: 'POST',
            data: form_data,
            dataType: 'json'
        })
        .done(function(res) {
            
            console.log('Respuesta facial');
            console.log(res);            
            var data = res && res['status'] ? res['data']: null;
            console.log(data);

            if (data && data.status == '200') {

                console.log('200');                
                $('#token-value').text(data.toc_token);
                $('#firstname').text(data.name);
                $('#lastname').text(data.lastname);
                
                mathFacial(data['biometric result'], data['front_reniec_result'])                

                $('#result-tab').tab('show');
                $('.loading').hide();
                $('.result').show();
                $('.info-doc').show();

                if(signDocument){
                    signDocumentFN(2, data.toc_token, null);
                }                

            } else {                                                               
                    console.log('<> 200');
                    if (data.status == '201') {
                        displayErrorApi(false, false, false);
                    } else if (data.status == '202') {
                        displayErrorApi(true, true, false);
                    } else if (data.status == '203') {
                        displayErrorApi(false, false, false);
                    } else if (data.status == '431') {
                        displayErrorApi(false, false, false);
                    }
                    else if(data.status == '417'){
                        console.log('!DNI');
                        displayErrorApi(true, true, false);
                    }
                    else if(data.status == '411' || data.status == '416' || data.status == '410' || data.status == '411'){                                                
                        console.log('Faltan parámetros');
                        displayErrorApi(false, false, false);                                        
                    } else {                    
                        if(_liveness){
                            bootbox.alert('Ocurrió un problema por favor, vuelva a iniciar el proceso');
                        }
                        else{
                            displayErrorApi(false, false);
                        }
                    }                    
                                
                    $('.loading').hide();
            }
        }).fail(function(data) {
            console.log(data);
            console.log("error");
            // TODO: códigos
            bootbox.alert('Ocurrió un error al consultar la API Facial. Por favor, inténtelo nuevamente.');
            $('.loading').hide();
        });
    } else {        
        $('.loading').hide();
        bootbox.alert('Por de las 2 imágenes no tiene datos válidos.');
    }

}



$('body').on('click', '.api-call', function(event) {    
    event.preventDefault();
    callAPI();
});



