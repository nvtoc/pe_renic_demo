var video = document.querySelector('video');
var streamID = null;

// Función que actualiza números de los pasos del proceso
function setNavigationNumbers() {
    console.log('setNavigationNumbers FN');
    var liTabs = $('#apiTab > li:visible');
    $.each(liTabs, function(index, el) {
        $(this).children('a').children('span').text(index + 1);
    });
}

// Función que define los pasos del proceso según tipo de documento
function setAppSteps() {
    var documentType = $('#documentType').val();
    
    console.log('setAppSteps FN'+ documentType);        
    switch (documentType) {
        case 'ONLYFACE':
            $('#document-tab').parent().hide();
            $('#back-tab').parent().hide();
            $('fieldset#front').children('h2').text(lang[app_locale]['selfie_title']);
            $('fieldset#front').children('h3').text(lang[app_locale]['selfie_subtitle']);
            if (DetectRTC.isMobileDevice) {
                $('fieldset#front').find('img#id_front').attr('src', 'img/selfie.png');
                $('fieldset#front').children('img.check-orientation').attr('src', 'img/selfie-ok.png');
            }
            $('fieldset#front').children('h3').text(lang[app_locale]['selfie_subtitle']);
        break;
        case 'PASS':
            $('#document-tab').parent().show();
            $('#back-tab').parent().hide();
        break;               
        default:
            if(!documentKind){
                $('#back-tab').parent().show();
                $('#document-tab').parent().show();
                $('fieldset#front').children('h2').text(lang[app_locale]['document_front_title']);
                $('fieldset#front').children('h3').text(lang[app_locale]['document_front_subtitle']);
            }            
    }
    setNavigationNumbers();
}

// Función que bloquea fieldset por validación
function blockFieldset(id, message) {
    $('fieldset' + id).shake({interval: 50, distance: 15, times: 5});
    bootbox.alert(message);
}

// Función que actualiza el stream de video para sacar fotos
function refreshVideoStream(id, retake) {

    console.log('refreshVideoStream FN');
    // Mover elemento video para el streaming
    if ($(id + '-video' + ' #video-stream').length == 0) {
        $(id + '-video').append(document.getElementById('video-stream'));
    }

    // Mostrar camaras
    if (!DetectRTC.isMobileDevice) {                  
        if ($('#videoSource option').length > 1) {
            $('.cameras').show();
        } else {
            $('.cameras').hide();
        }              
    }

    // Preview imagen
    if ($(id + '-video').siblings('img').attr('src').length == 0 || retake) {
        ($('#video-stream').is(':hidden')) ? $('#video-stream').show() : '';
        $('.btn.rotate').hide();
        $(id + '-video').siblings('img').hide();
        $('fieldset' + id).children('.take-photo').removeClass('retake').children('span').text(lang[app_locale]['photo']);
    } else {
        ($('#video-stream').is(':visible')) ? $('#video-stream').hide() : '';
        $('.btn.rotate').show();
        $('.next').show();
        $(id + '-video').siblings('img').show();
    }
}


// Función que despliega las fotos que no se procesaron correctamente resultados
function displayErrorApi(frontStatus, selfieStatus, dniStatus) {
    errorCount = 0;
    ok_class = 'fa fa-check fa-fw text-success';
    error_class = 'fa fa-times fa-fw text-danger';

    $('#error_facial .modal-body ul.errorPhoto li span i').removeClass();
    if (frontStatus) {
        $('#error_facial .modal-body ul.errorPhoto li.front i').addClass(ok_class);
        $('#error_facial .modal-body ul.errorPhoto li.front a').hide();
    } else {
        $('#error_facial .modal-body ul.errorPhoto li.front i').addClass(error_class);
        $('#error_facial .modal-body ul.errorPhoto li.front a').show();
        errorCount++;
    }   
    if (selfieStatus) {
        $('#error_facial .modal-body ul.errorPhoto li.selfie i').addClass(ok_class);
        $('#error_facial .modal-body ul.errorPhoto li.selfie a').hide();
    } else {
        $('#error_facial .modal-body ul.errorPhoto li.selfie i').addClass(error_class);
        $('#error_facial .modal-body ul.errorPhoto li.selfie a').show();
        errorCount++;
    }

    if(typeof dniStatus !== undefined){
        $('#error_facial .modal-body ul.errorPhoto li.dni a').show();
    }

    $('#errorPhotoCount').text(errorCount);
    $( '#error_facial').modal('show');
}


// Función que rota una imagen base64
var rotate64 = function(base64data, degrees, enableURI) {
    console.log('rotate64 FN');
    return new Promise(function(resolve, reject) {
        //assume 90 degrees if not provided
        var degrees = 90;
        if (direction == 'left') {
            degrees = -90;
        }
        var canvas = document.createElement('canvas');
        canvas.setAttribute('id', 'hidden-canvas');
        canvas.style.display = 'none';
        document.body.appendChild(canvas);
        var ctx = canvas.getContext('2d');
        var image = new Image();
        //assume png if not provided
        image.src = base64data;
        image.onload = function() {
            var w = image.width;
            var h = image.height;
            var rads = degrees * Math.PI/180;
            var c = Math.cos(rads);
            var s = Math.sin(rads);
            if (s < 0) { s = -s; }
            if (c < 0) { c = -c; }
            //use translated width and height for new canvas
            canvas.width = h * s + w * c;
            canvas.height = h * c + w * s;
            //draw the rect in the center of the newly sized canvas
            ctx.translate(canvas.width/2, canvas.height/2);
            ctx.rotate(degrees * Math.PI / 180);
            ctx.drawImage(image, -image.width/2, -image.height/2);
            //assume plain base64 if not provided
            resolve(canvas.toDataURL('image/jpeg'));
            document.body.removeChild(canvas);
        };
        image.onerror = function() {
            reject('Unable to rotate data\n' + image.src);
        };
    });
}
// Función que transforma una imagen base64 como url a archivo
function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
    byteString = atob(dataURI.split(',')[1]);
    else
    byteString = decodeURIComponent(dataURI.split(',')[1]);
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ia], {type:'image/jpeg'});
}


function stopVideoStream(){    
    console.log('StopVideoStream FN');
    var stream = video.srcObject;    
    if(stream){
        var tracks = stream.getTracks();  
        tracks.forEach(function(track) {
          track.stop();
        });  
    }
    video.srcObject = null;
}

// Función que establece la conexión de video con la cámara seleccionada
function setVideoStream(deviceId) {   
    console.log('setVideoStream FN');
    if(deviceId != streamID){
        stopVideoStream();    
        streamID = deviceId;
        var constraints = { 
            audio: false, 
            video: {
                width: { min: 800, ideal: 1280, max: 1280 },
                height: { min: 600, ideal: 720, max: 720 },
                deviceId: deviceId
            }
        };    
    
        navigator.mediaDevices.getUserMedia(constraints).then(function(mediaStream) {        
            // var preview = $('video#preview');
            video.srcObject = mediaStream;
            video.onloadedmetadata = function(e) {
                video.play();
            };
        }).catch(function(err) {
            bootbox.alert('Ocurrió un error al cambiar de cámara, reconecte al equipo para una correcta identificación y recargue la página.')
            console.log(err.name + ": " + err.message);
        }); // always check for errors at the end.
    }    
}



// Función que despliega los tipos de documentos por país
function setCountryOptions(country, locale) {
    console.log('setCountryOptions FN');
    $('#documentType').find('option').remove();
    var ci_codes = '';
    if (locale === 'es') {
        ci_codes = ci_codes_es;
    } else if (locale === 'en') {
        ci_codes = ci_codes_en;
    }
    if (typeof country != 'undefined' && country != '' && country != null && country.length > 0) {
        $('label[for="documentType"]').text(lang[app_locale]['document_type'] + ' ' + country);
        // set especific options
        $.each(ci_codes, function(index, element) {
            if (element.hasOwnProperty(country)) {
                $.each(element[country], function(index, item) {
                    $('#documentType').append($('<option>', {
                        value: item.value,
                        text : item.name
                    }));
                });
            }
        });
    } else {
        // set all options
        $('label[for="documentType"]').text(lang[app_locale]['document_type'] + ' : ');
        $('#documentType').append($('<option>', {
            value: 0,
            text : lang[app_locale]['select_document']
        }));
        $.each(ci_codes, function(index, element) {
            for (var key in element) {
                $.each(element[key], function(index, item) {
                    if (item.value != 0) {
                        $('#documentType').append($('<option>', {
                            value: item.value,
                            text : item.name + ' ' + key
                        }));
                    }
                });
            }
        });
    }
}
// Función que configura el lenguage de la aplicación
function setLanguage() {
    console.log('setLanguage FN');
    var language = navigator.language.split('-');
    // $('button[data-lang="' + language[0] + '"]').addClass('btn-dark');
    // setLocale(language[0]);
    // return language[0];
    $('button[data-lang="' + 'es' + '"]').addClass('btn-dark');
    setLocale('es');
    return 'es';
}
// Función que traduce la página en base al lenguaje seleccionado
function setLocale(locale) {
    console.log('setLocale FN');
    $('input[name="lang"]').val(locale);
    $('.checkbox .col-form-label span').text(' ' + lang[locale]['all_documents']);
    var path_prefix = 'locale/';
    if (window.location.href.indexOf('liveness') >= 0) {
        path_prefix = '../locale/';
    }
    var opts = { language: locale, pathPrefix: path_prefix, skipLanguage: 'es-CL' };
    $('[data-localize]').localize('app', opts);
    getGeoCountry(locale);
}
// Función que despliega los templates de los documentos por país
function setTemplateCountry(country = '') {
    console.log('setTemplateCountry FN');
    var base_image = '';    
    switch (country) {
        case 'Chile':
            $('img#id_front').attr({src: base_image + 'ci_front.png'});
            $('img#id_back').attr({src: base_image + 'ci_back.png'});
        break;
        case 'Peru':
            $('img#id_front').attr({src: base_image + 'dni_front_peru.png'});
            $('img#id_back').attr({src: base_image + 'dni_back_peru.png'});
        break;
        case 'Colombia':
            $('img#id_front').attr({src: base_image + 'ci_front_col.png'});
            $('img#id_back').attr({src: base_image + 'ci_back_col.png'});
        break;
        default:
            $('img#id_front').attr({src: base_image + 'front.png', style: 'margin: 1%;width:35%;'});
            $('img#id_back').attr({src: base_image + 'back.png', style: 'margin: 1%;width:35%;'});
    }
    $('img#selfie').attr({src: base_image + 'selfie.png', style: 'margin: 1%;width:20%;'});
}
// Función que saca la foto para versión desktop
function takePhoto() {
    console.log('takePhoto FN');
    var video = document.getElementById('video');
    var canvas = document.getElementById('photo');
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
    if (video.videoWidth < 1280) {
      canvas.getContext('2d').drawImage(video, 0, 0, video.videoWidth, video.videoHeight);
    } else {
      canvas.getContext('2d').drawImage(video, 0, 0, 1280, 720, 0, 0, 1280, 720);
    }
    return canvas.toDataURL('image/jpeg');
}
// Función que busca la ubicación geografica del usuario
function getGeoCountry(locale) {
    console.log('takePhoto FN');
    var country = '';
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            $.ajax('https://maps.googleapis.com/maps/api/geocode/json?latlng='
            + position.coords.latitude + ','
            + position.coords.longitude)
            .done(function(data, textStatus, jqXHR) {
                if ( textStatus === 'success' && typeof data.results != 'undefined') {
                    var country = null, countryCode = null;
                    for (var r = 0, rl = data.results.length; r < rl; r += 1) {
                        var result = data.results[r];
                        if (!country && result.types[0] === 'country') {
                            country = result.address_components[0].long_name;
                            countryCode = result.address_components[0].short_name;
                        }
                    }
                }
                console.log(country);
                if (DetectRTC.isMobileDevice) {
                    // setTemplateCountry(country);
                }
                setCountryOptions(country, locale);
            });
        });
    } else {
        if (DetectRTC.isMobileDevice) {
            // setTemplateCountry(country);
        }
        setCountryOptions(country, locale);
    }
}

/*!
* jQuery UI Effects Shake 1.11.4
* http://jqueryui.com
*
* Copyright jQuery Foundation and other contributors
* Released under the MIT license.
* http://jquery.org/license
*
* http://api.jqueryui.com/shake-effect/
*/

(function($){
    $.fn.shake = function(settings) {
        if (typeof settings.interval == 'undefined') {
            settings.interval = 100;
        }
        if( typeof settings.distance == 'undefined') {
            settings.distance = 10;
        }
        if( typeof settings.times == 'undefined') {
            settings.times = 4;
        }
        if( typeof settings.complete == 'undefined') {
            settings.complete = function(){};
        }
        $(this).css('position','relative');
        for (var iter=0; iter<(settings.times+1); iter++) {
            $(this).animate({ left:((iter%2 == 0 ? settings.distance : settings.distance * -1)) }, settings.interval);
        }
        $(this).animate({ left: 0}, settings.interval, settings.complete);
    };
})(jQuery);


$("body").on("change", 'input[type="file"]', function(e) {
    e.preventDefault();    
    $('.btn.rotate').show();
});
