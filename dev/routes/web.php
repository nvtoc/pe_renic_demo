<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('pages.index');
// });


Route::get('/', 'PageController@index');
Route::post('/pages/liveness','PageController@facial');


Route::post('/pages/sign_document','PageController@_sign_document');
Route::post('/pages/facial_data','PageController@_facial_data');
Route::post('/pages/register_fail','PageController@_register_fail');

