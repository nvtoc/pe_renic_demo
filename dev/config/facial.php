<?php
    return [
    'URL_SIGNER' => env('URL_SIGNER'),
    'FACIAL_API_KEY' => env('FACIAL_API_KEY'),
    'URL_FACE' => env('URL_FACE'),
];