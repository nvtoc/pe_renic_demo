# Biometría Facial/Reniec

## Descripción

- Demo de verificación Facial de Selfie contra Reniec.
- Sofware del demo: Laravel 5.8
- Lenguajes PHP >= 7.1, JS, HTML


## Requerimientos
- Servidor Web
- PHP
- Composer

## Instalación

- Clonar repositorio
- Ir a dev/
- Correr composer install
> composer install
- Generar llave
> php artisan key:generate

- Clonar .env.template a .env
> cp .env.template .env
- Configurar las variables en .env 
    - FACIAL_API_KEY=
    - URL_SIGNER=
    - URL_FACE=
    - TOKEN_LIVENESS= 
    - Para prubas localhost user
        - APP_ENV=local => genera ruta http:
        - En otro caso APP_ENV=prod  => genera ruta https

- Para https cambiar url por secure_url
    - resources/view/elements/footer_scripts.blade.php por secure_url
    - Ejemplo secure_url('path_to_link')


